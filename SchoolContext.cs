﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDAL
{
    public class SchoolContext:DbContext
    {
        public SchoolContext() : base()
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>().HasKey<int>(s => s.StudentId);
            modelBuilder.Entity<Student>().Property(s => s.StudentName).IsRequired();
            modelBuilder.Entity<Grade>().HasKey<int>(s => s.GradeId);
            modelBuilder.Entity<Grade>().Property(s => s.GradeName).IsRequired();
            modelBuilder.Entity<Grade>().Property(s => s.Section).IsRequired();
        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Grade> Grades { get; set; }

    }
}
